INSERT INTO address VALUES
(null,'street1','city1','province1','country1','12345'),
(null,'street2','city2','province2','country2','67890')
;

INSERT INTO manufacturer VALUES
(null,'manufacturer1',1),
(null,'manufacturer2',2)
;

INSERT INTO brand VALUES
(null,'brand1'),
(null,'brand2')
;

INSERT INTO product VALUES
(null,'artnumber1','product1','product description from product1',1,1,0),
(null,'artnumber2','product2','product description from product2',1,2,0),
(null,'artnumber3','product3','product description from product3',2,1,0),
(null,'artnumber4','product4','product description from product4',2,2,0),
(null,'artnumber5','product5','product description from product5',1,2,10),
(null,'artnumber6','product6','product description from product6',1,1,15),
(null,'artnumber7','product7','product description from product7',2,1,12),
(null,'artnumber8','product8','product description from product8',2,2,14),
(null,'artnumber9','product9','product description from product9',1,1,8),
(null,'artnumber10','product10','product description from product10',1,2,9)
;

INSERT INTO valuta VALUES
(null, 'USD','United States Dollar'),
(null, 'IDR','Indonesia Rupiah')
;

INSERT INTO price VALUES
(null, 1, 1, 1),
(null, 1, 2, 14000),
(null, 2, 1, 2),
(null, 2, 2, 28000),
(null, 3, 1, 1.5),
(null, 3, 2, 21000),
(null, 4, 1, 1.6),
(null, 4, 2, 22400),
(null, 5, 1, 2.5),
(null, 5, 2, 35000),
(null, 6, 1, 3),
(null, 6, 2, 42000),
(null, 7, 1, 2.2),
(null, 7, 2, 30800),
(null, 8, 1, 1.55),
(null, 8, 2, 21700),
(null, 9, 1, 1.05),
(null, 9, 2, 14700),
(null, 10, 1, 2.15),
(null, 10, 2, 30100)
;