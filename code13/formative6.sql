SELECT
p.name as productName,
p.stock as productStock,
m.name as manufactureName,
a.street as manufactureStreet,
a.province as manufactureProvince,
a.country as manufactureCountry,
a.zipcode as manufactureZipCode,
v.code as currency,
pr.amount as priceAmount
FROM PRODUCT as p
JOIN manufacturer as m
ON p.manufactureid = m.id
JOIN address as a
ON m.addressid = a.id
JOIN price as pr
ON p.id = pr.productid
JOIN valuta as v
ON pr.valutaid = v.id
GROUP BY v.code
;