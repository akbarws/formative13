SELECT p.artNumber as productArtNumber, 
p.name as productName,
p.stock as productStock,
b.name as brandName,
m.name as manufactureName,
a.street as manufactureStreet,
a.city as manufactureCity,
a.province as manufactureProvince,
a.country as manufactureCountry,
a.zipcode as manufactureZipCode,
GROUP_CONCAT(concat(v.code, ' ', pr.amount) SEPARATOR ',') as productPrices
FROM product as p 
JOIN manufacturer as m 
ON p.manufactureid = m.id 
JOIN address as a
ON m.addressid = a.id
JOIN price as pr
ON p.id = pr.productid
JOIN brand as b
ON p.brandid = b.id
JOIN valuta as v
ON pr.valutaid = v.id
WHERE p.stock = 0
GROUP BY p.name;