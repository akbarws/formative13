CREATE TABLE address(
    id INTEGER NOT NULL AUTO_INCREMENT,
    street VARCHAR(100) NOT NULL,
    city VARCHAR(50) NOT NULL,
    province VARCHAR(50) NOT NULL,
    country VARCHAR(50) NOT NULL,
    zipcode VARCHAR(5) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE manufacturer(
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    addressid INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (addressid) REFERENCES address(id)
);

CREATE TABLE brand(
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product(
    id INTEGER NOT NULL AUTO_INCREMENT,
    artNumber VARCHAR(100) NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(200) NOT NULL,
    manufactureid INT NOT NULL,
    brandid INT NOT NULL,
    stock INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (manufactureid) REFERENCES manufacturer(id),
    FOREIGN KEY (brandid) REFERENCES brand(id)
);

CREATE TABLE valuta(
    id INTEGER NOT NULL AUTO_INCREMENT,
    code VARCHAR(50) NOT NULL,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE price(
    id INTEGER NOT NULL AUTO_INCREMENT,
    productid INTEGER NOT NULL,
    valutaid INTEGER NOT NULL,
    amount INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (productid) REFERENCES product(id),
    FOREIGN KEY (valutaid) REFERENCES valuta(id)
);